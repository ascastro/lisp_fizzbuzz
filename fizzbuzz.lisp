#! /usr/local/bin/clisp

(defun fizzbuzz-main ()
  (let ((n (parse-integer (or (car *args*) "20"))))
    (loop for i from 1 to n do
      (fizzbuzz i))
    (quit)))

; (defun fizzbuzz (n)
;   (when (zerop (mod n 3)) (format t "Fizz"))
;   (when (zerop (mod n 5)) (format t "Buzz"))
;   (unless (or (zerop (mod n 3)) (zerop (mod n 5))) (format t "~D" n))
;   (format t "~%"))

(defun fizzbuzz (n)
  (format t "~A~%" (cond
    ((and (zerop (mod n 3)) (zerop (mod n 5))) "FizzBuzz")
    ((zerop (mod n 3)) "Fizz")
    ((zerop (mod n 5)) "Buzz")
    (t n))))

(fizzbuzz-main)
